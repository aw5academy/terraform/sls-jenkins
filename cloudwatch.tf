resource "aws_cloudwatch_log_group" "ecs-task" {
  name              = "/aws/ecs/${local.app-name}"
  retention_in_days = "14"
  tags = {
    Name = "/aws/ecs/${local.app-name}"
  }
}

resource "aws_cloudwatch_log_group" "codebuild" {
  name              = "/aws/codebuild/${local.app-name}"
  retention_in_days = "14"
  tags = {
    Name = "/aws/codebuild/${local.app-name}"
  }
}
