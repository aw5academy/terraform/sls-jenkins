resource "aws_efs_file_system" "jenkins" {
  creation_token = local.app-name
  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }
  tags = {
    Name = local.app-name
  }
}

resource "aws_efs_mount_target" "jenkins-private-a" {
  file_system_id  = aws_efs_file_system.jenkins.id
  security_groups = [aws_security_group.efs.id]
  subnet_id       = module.vpc.private-a-subnet-id
}

resource "aws_efs_mount_target" "jenkins-private-b" {
  file_system_id  = aws_efs_file_system.jenkins.id
  security_groups = [aws_security_group.efs.id]
  subnet_id       = module.vpc.private-b-subnet-id
}

resource "aws_efs_access_point" "jenkins" {
  file_system_id = aws_efs_file_system.jenkins.id
  posix_user {
    gid = "1000"
    uid = "1000"
  }
  root_directory {
    creation_info {
      owner_gid = "1000"
      owner_uid = "1000"
      permissions = "0777"
    }
    path = "/jenkins-home"
  }
}
