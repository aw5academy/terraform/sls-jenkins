resource "aws_ecs_cluster" "app" {
  name = local.app-name
  tags = {
    Name = local.app-name
  }
}

resource "aws_ecs_service" "app" {
  name                              = local.app-name
  cluster                           = aws_ecs_cluster.app.id
  task_definition                   = aws_ecs_task_definition.app.arn
  desired_count                     = "1"
  enable_execute_command            = "true"
  health_check_grace_period_seconds = "900"
  launch_type                       = "FARGATE"
  platform_version                  = "1.4.0"
  depends_on                        = [aws_lb.ecs]
  load_balancer {
    target_group_arn = aws_lb_target_group.ecs["blue"].arn
    container_name   = local.app-name
    container_port   = "8080"
  }
  network_configuration {
    assign_public_ip = false
    security_groups  = [aws_security_group.ecs.id, aws_security_group.efs.id]
    subnets          = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
  }
  tags = {
    Name = local.app-name
  }
}
