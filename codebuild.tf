resource "aws_codebuild_project" "small" {
  artifacts {
    type = "NO_ARTIFACTS"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    environment_variable {
      name = "AWS_ACCOUNT_ID"
      value = data.aws_caller_identity.current.account_id
    }
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }
  logs_config {
    cloudwatch_logs {
      group_name = aws_cloudwatch_log_group.codebuild.name
    }
  }
  name          = "${local.app-name}-small"
  service_role  = aws_iam_role.codebuild.arn
  source {
    type            = "S3"
    location        = "${aws_s3_bucket.codebuild.id}/source.zip"
  }
  tags = {
    Name = "${local.app-name}-small"
  }
  vpc_config {
    vpc_id             = module.vpc.vpc-id
    subnets            = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
    security_group_ids = [aws_security_group.codebuild.id]
  }
}
