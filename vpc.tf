module "vpc" {
  source   = "git::https://gitlab.com/aw5academy/terraform/modules/vpc.git"
  vpc-name = local.app-name
  ssm      = "true"
}
