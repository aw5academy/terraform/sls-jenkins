################################################
# ALB Security Group
################################################
resource "aws_security_group" "alb" {
  description = "${local.app-name}-alb"
  name        = "${local.app-name}-alb"
  tags = {
    Name = "${local.app-name}-alb"
  }
  vpc_id = module.vpc.vpc-id
}

data "external" "ifconfig" {
  program = ["bash", "${path.module}/ifconfig.sh"]
}

resource "aws_security_group_rule" "alb-inbound" {
  cidr_blocks              = [data.external.ifconfig.result["public_ip"]]
  description              = "Allowing HTTP access"
  from_port                = "80"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.alb.id
  to_port                  = "80"
  type                     = "ingress"
}

resource "aws_security_group_rule" "alb-codebuild" {
  description              = "Allow HTTP traffic from CodeBuild."
  from_port                = "80"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.alb.id
  source_security_group_id = aws_security_group.codebuild.id
  to_port                  = "80"
  type                     = "ingress"
}

resource "aws_security_group_rule" "alb-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.alb.id
  to_port           = 0
  type              = "egress"
}

################################################
# ECS Security Group
################################################
resource "aws_security_group" "ecs" {
  description = "${local.app-name}-ecs"
  name        = "${local.app-name}-ecs"
  tags = {
    Name = "${local.app-name}-ecs"
  }
  vpc_id = module.vpc.vpc-id
}

resource "aws_security_group_rule" "ecs-alb" {
  description              = "Allow HTTP traffic from ALB."
  from_port                = 8080
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ecs.id
  source_security_group_id = aws_security_group.alb.id
  to_port                  = 8080
  type                     = "ingress"
}

resource "aws_security_group_rule" "ecs-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.ecs.id
  to_port           = 0
  type              = "egress"
}

################################################
# CodeBuild Security Group
################################################
resource "aws_security_group" "codebuild" {
  description = "${local.app-name}-codebuild"
  name        = "${local.app-name}-codebuild"
  tags = {
    Name = "${local.app-name}-codebuild"
  }
  vpc_id = module.vpc.vpc-id
}

resource "aws_security_group_rule" "codebuild-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.codebuild.id
  to_port           = 0
  type              = "egress"
}

################################################
# EFS Security Group
################################################
resource "aws_security_group" "efs" {
  description = "${local.app-name}-efs"
  name        = "${local.app-name}-efs"
  tags = {
    Name = local.app-name
  }
  vpc_id = module.vpc.vpc-id
}

resource "aws_security_group_rule" "efs-ingress" {
  description              = "Allowing EFS access"
  from_port                = "2049"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.efs.id
  source_security_group_id = aws_security_group.efs.id
  to_port                  = "2049"
  type                     = "ingress"
}

resource "aws_security_group_rule" "efs-egress" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allowing full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.efs.id
  to_port           = 0
  type              = "egress"
}
