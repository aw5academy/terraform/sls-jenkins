resource "aws_ecs_task_definition" "app" {
  family                   = local.app-name
  container_definitions    = templatefile("${path.module}/templates/container-definitions.json.tpl", {
                               app        = local.app-name,
                             })
  execution_role_arn       = aws_iam_role.ecs-task-exec.arn
  task_role_arn            = aws_iam_role.ecs-tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "1024"
  memory                   = "4096"
  volume {
    name = "jenkins-home"

    efs_volume_configuration {
      file_system_id          = aws_efs_file_system.jenkins.id
      transit_encryption      = "ENABLED"
      authorization_config {
        access_point_id = aws_efs_access_point.jenkins.id
        iam             = "ENABLED"
      }
    }
  }
}
