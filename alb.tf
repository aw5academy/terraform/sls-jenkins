resource "aws_lb" "ecs" {
  internal           = false
  load_balancer_type = "application"
  name               = local.app-name
  security_groups    = [aws_security_group.alb.id]
  subnets            = [module.vpc.public-a-subnet-id, module.vpc.public-b-subnet-id]
  tags = {
    Name = local.app-name
  }
}

resource "aws_lb_listener" "ecs" {
  default_action {
    target_group_arn = aws_lb_target_group.ecs["blue"].arn
    type             = "forward"
  }
  lifecycle {
    ignore_changes = [
      default_action
    ]
  }
  load_balancer_arn = aws_lb.ecs.arn
  port              = "80"
  protocol          = "HTTP"
}

resource "aws_lb_target_group" "ecs" {
  for_each             = toset(["blue"])
  name                 = "${local.app-name}-${each.value}"
  port                 = "8080"
  protocol             = "HTTP"
  vpc_id               = module.vpc.vpc-id
  target_type          = "ip"
  health_check {
    healthy_threshold   = "2"
    interval            = "60"
    matcher             = "200"
    path                = "/login"
    port                = "8080"
    protocol            = "HTTP"
    timeout             = "5"
    unhealthy_threshold = "5"
  }
  tags = {
    Name = "${local.app-name}-${each.value}"
  }
}
