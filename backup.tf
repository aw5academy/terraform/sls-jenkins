data "aws_kms_key" "backup" {
  key_id = "alias/aws/backup"
}

resource "aws_backup_vault" "jenkins" {
  name        = local.app-name
  kms_key_arn = data.aws_kms_key.backup.arn
  tags = {
    Name = local.app-name
  }
}

resource "aws_backup_plan" "jenkins" {
  name = local.app-name
  rule {
    rule_name         = "${local.app-name}-daily"
    target_vault_name = aws_backup_vault.jenkins.name
    schedule          = "cron(0 3 * * ? *)"
    lifecycle {
      delete_after = "30"
    }
  }
  tags = {
    Name = local.app-name
  }
}

resource "aws_backup_selection" "jenkins" {
  iam_role_arn = aws_iam_role.backup.arn
  name         = local.app-name
  plan_id      = aws_backup_plan.jenkins.id
  resources = [
    aws_efs_file_system.jenkins.arn
  ]
}
