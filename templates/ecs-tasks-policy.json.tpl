{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ssmmessages:CreateControlChannel",
        "ssmmessages:CreateDataChannel",
        "ssmmessages:OpenControlChannel",
        "ssmmessages:OpenDataChannel"
      ],
      "Resource": "*"
    },
    {
      "Effect":"Allow",
      "Resource":[
        "${codebuild_arn}"
      ],
      "Action":[
        "codebuild:StartBuild",
        "codebuild:BatchGetBuilds",
        "codebuild:BatchGetProjects"
      ]
    },
    {
      "Effect":"Allow",
      "Resource":[
        "${logs_arn}:*"
      ],
      "Action":[
        "logs:GetLogEvents"
      ]
    },
    {
      "Effect":"Allow",
      "Resource":[
        "${bucket_arn}",
        "${bucket_arn}/*"
      ],
      "Action":[
        "s3:PutObject",
        "s3:GetObject",
        "s3:GetBucketVersioning"
      ]
    }
  ]
}
