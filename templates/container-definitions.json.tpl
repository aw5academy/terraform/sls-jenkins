[
  {
    "name": "${app}",
    "image": "jenkins/jenkins:lts-jdk11",
    "linuxParameters": {
      "initProcessEnabled": true
    },
    "environment": [
      {
        "name": "JENKINS_HOME",
        "value": "/mnt/efs"
      }
    ],
    "essential": true,
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "/aws/ecs/${app}",
            "awslogs-region": "us-east-1",
            "awslogs-stream-prefix": "stream"
        }
    },
    "portMappings": [
      {
        "protocol": "tcp",
        "containerPort": 8080
      }
    ],
    "mountPoints": [
      {
        "readOnly": false,
        "containerPath": "/mnt/efs",
        "sourceVolume": "jenkins-home"
      }
    ]
  }
]
