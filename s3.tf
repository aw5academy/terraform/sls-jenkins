resource "random_string" "random" {
  length  = 12
  special = false
  upper   = false
  number  = false
}

resource "aws_s3_bucket" "codebuild" {
  bucket        = "${local.app-name}-${random_string.random.result}"
  acl           = "private"
  force_destroy = true
  tags = {
    Name = "${local.app-name}-${random_string.random.result}"
  }
  versioning {
    enabled = true
  }
}
