data "aws_iam_policy_document" "ecs-tasks" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

################################################
# ECS TASK EXECUTION ROLE
################################################
resource "aws_iam_role" "ecs-task-exec" {
  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
  name               = "${local.app-name}-ecs-task-exec"
  tags = {
    Name = "${local.app-name}-ecs-task-exec"
  }
}

resource "aws_iam_role_policy_attachment" "amazon-ecs-task-exec" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.ecs-task-exec.name
}

################################################
# ECS TASKS ROLE
################################################
resource "aws_iam_role" "ecs-tasks" {
  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
  name               = "${local.app-name}-ecs"
  tags = {
    Name = "${local.app-name}-ecs"
  }
}

resource "aws_iam_role_policy" "ecs-tasks" {
  name   = "ecs-tasks"
  policy = templatefile("${path.module}/templates/ecs-tasks-policy.json.tpl", 
    {
      codebuild_arn = aws_codebuild_project.small.arn
      logs_arn      = aws_cloudwatch_log_group.codebuild.arn
      bucket_arn    = aws_s3_bucket.codebuild.arn
    }
  )
  role   = aws_iam_role.ecs-tasks.id
}

################################################
# CODEBUILD ROLE
################################################
data "aws_iam_policy_document" "codebuild" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codebuild" {
  assume_role_policy = data.aws_iam_policy_document.codebuild.json
  name               = "${local.app-name}-codebuild"
  tags = {
    Name = "${local.app-name}-codebuild"
  }
}

resource "aws_iam_role_policy" "codebuild" {
  name   = "CodeBuildBase"
  policy = templatefile("${path.module}/templates/codebuild-policy.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      bucket_arn     = aws_s3_bucket.codebuild.arn
    }
  )
  role  = aws_iam_role.codebuild.id
}

################################################
# BACKUP ROLE
################################################
data "aws_iam_policy_document" "backup" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "backup" {
  assume_role_policy = data.aws_iam_policy_document.backup.json
  name               = "${local.app-name}-backup"
  tags = {
    Name = "${local.app-name}-backup"
  }
}

resource "aws_iam_role_policy_attachment" "backup" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.backup.name
}
